# Fear Machine

Heavily inspired by https://github.com/ftrain/anxietybox 
If we automate everything, why not automate fear? At least this way, when it's an email, it's easy to see it for what it is. Inside the mind, it's a different story.